package cr.ac.ucenfotec;

import javax.swing.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Conector{
    private static AccesoBD connectorBD = null;
    public static AccesoBD getConector(){
        if(connectorBD==null){
            try {
                String[] propertiesReader;
                PropsValuesReader propertiesInstance = new PropsValuesReader();
                propertiesReader = propertiesInstance.getPropValues();
                connectorBD = new AccesoBD(
                        propertiesReader[0],
                        propertiesReader[1]
                );
                System.out.println("DataBase Connection Succeed");
            }catch (Exception exception){
                exception.printStackTrace();
            }
        }
        return connectorBD;
    }
}